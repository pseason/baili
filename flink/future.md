# 部署

###  kafka-monitor
```sh
java -cp kafka-monitor.jar com.quantifind.kafka.offsetapp.OffsetGetterWeb --zk localhost:2181 --port 9204 --refresh 10.seconds --retain 2.days
```

### java -jar 启动
```sh
java -Xmx48m -Xms16m -Xmn8m -server -jar filebeats-output-1.0-OFFICIAL.jar
```

### kafka consumer listener
```sh
  bin/kafka-console-consumer.sh --bootstrap-server 192.168.0.224:9092 --topic filebeat --from-beginning
```

### kafka producer command
```sh
bin/kafka-console-producer.sh --broker-list 192.168.0.224:9092 --topic filebeat
```

### java
```sh
java -cp target\consumer-kafka-1.0-OFFICIAL.jar com.baili.flink.run.FlinkExampleKafka --input-topic filebeat --output-topic filebeat-out --bootstrap.servers 192.168.0.224:9092 --zookeeper.connect 192.168.0.224:2181 --group.id flink-example-kafka
```

### zookeeper启动
```sh
zkServer.sh start &
```

### kafka 启动
```sh
bin/kafka-server-start.sh config/server.properties &
```
