#!/usr/bin/env python
# coding=utf-8

import argparse
import distutils.util
import json
import logging
import time

import colorlog
import psutil
import pymysql.cursors
from sarge import run, Capture

from util import *

os.chdir(os.path.dirname(os.path.abspath(__file__)))

format = '%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s'
logging.basicConfig(level=logging.INFO,
                    format=format,
                    filename=os.path.basename(os.path.abspath(__file__))[:-2] + "log"
                    )

handler = colorlog.StreamHandler(sys.stdout)
handler.setFormatter(colorlog.ColoredFormatter(
    '%(log_color)s' + format))

logging.getLogger('').addHandler(handler)

parser = argparse.ArgumentParser()
parser.add_argument("-a", "--action", type=str,
                    choices=['get_info',
                             'stop_game', 'start_game',
                             'update_sql', 'update_game', 'update_hotfix',
                             'is_game_stop', 'is_game_start',
                             'md5_check'],
                    default='get_info',
                    help='''get_info:获取信息
stop_game:停止游戏服
start_game:启动游戏服
update_sql:更新SQL
update_game:更新游戏服
update_hotfix:热更新
is_game_stop:检查游戏服是否停止
is_game_start:检查游戏服是否启动
md5_check:md5检验
'''
                    )
parser.add_argument("-d", "--debug", type=distutils.util.strtobool,
                    default=False,
                    help="启用调试模式，忽略SQL数据，此模式优先级最高，默认是关闭调试模式，以SQL数据为准")
parser.add_argument("--assign_dir", type=str,
                    default="all",
                    help="默认是all，所有区服，可以指定一个或多个游戏服目录，以逗号分隔")
args = parser.parse_args()

action = args.action
debug = args.debug
assign_dir = args.assign_dir
assign_dir_list = []

if assign_dir == "all":
    pass
elif "," in assign_dir:
    assign_dir_list = assign_dir.split(",")
else:
    assign_dir_list.append(assign_dir)

hostname = socket.gethostname()
if debug:
    pass
else:
    if '.game.' in hostname:
        pass
    else:
        logging.error("%s:此服务器不是游戏服\n" % hostname)
        exit(1)

sql_list = ["select paramValue as `data` from s_server_setting where paramName='clientPort'",
            "select paramValue as `data` from s_server_setting where paramName='httpPort'",
            "select paramValue from s_server_setting where paramName='serverId'",
            "select paramValue from s_server_setting where paramName='serverName'",
            ]


def check_data(id_name):
    conn = pymysql.connect(
        host='gz-cdb-hhpuylx6.sql.tencentcdb.com',
        user='cdb_out_read',
        passwd='QLlS3im8w17A6tjH',
        port=63520,
        database='config',
        charset='utf8',
        use_unicode=True,
        cursorclass=pymysql.cursors.DictCursor)
    cursor = conn.cursor()

    lan_ip = get_lan_ip()

    cursor.execute("select * from list where (`hostname`=%s or `lan_ip`=%s) and `id_name`=%s and `server_status`='enable' limit 1", (hostname, lan_ip, id_name))
    data = cursor.fetchone()
    if data:
        return data
    else:
        return {}


def sarge_cmd(cmd, cwd=None):
    p = run(cmd, stdout=Capture(), async_=True, cwd=cwd)  # returns immediately
    logging.info("start_cmd:{0} cwd:{1}".format(cmd, cwd))

    p.close()
    line = p.stdout.readline()

    while line:
        # DO OTHER STUFF
        line = p.stdout.readline()
        logging.info(line)

    if p.returncode:
        logging.error("end_cmd:{0} return_code:{1} cwd:{2}".format(cmd, p.returncode, cwd))
    else:
        logging.info("end_cmd:{0} return_code:{1} cwd:{2}".format(cmd, p.returncode, cwd))


def run_file(cmd, game_dir):
    temp_file = os.path.join(game_dir, 'temp_file.sh')
    f = open(temp_file, 'w')
    f.write(cmd)
    f.close()
    sarge_cmd('bash temp_file.sh', cwd=game_dir)
    # os.remove(temp_file)


def check_game(config_file, tcp_port):
    proc_status = 0
    port_status = 0
    for proc in psutil.process_iter():
        try:
            if proc.name() == 'java' and proc.cwd() == os.path.dirname(config_file):
                proc_status = 1
                break
            else:
                continue
        except psutil.NoSuchProcess:
            continue

    for net in psutil.net_connections(kind='tcp'):
        if net.status == 'LISTEN' and net.laddr.port == int(tcp_port):
            port_status = 1
            break

    return (proc_status, port_status)


product = hostname.split('.')[2]


def update_sql(config_file):
    file_name = "%s.sql" % time.strftime("%Y%m%d")

    tmp_file = "/tmp/%s" % file_name

    if os.path.exists(tmp_file):
        os.remove(tmp_file)

    cmd = 'rsync -avP --password-file=/etc/rsync_user.secrets rsync_user@rsync.hundredcent.com::rsync_home/{product}/sql/{file_name} {tmp_file}'.format(
        product=product, file_name=file_name, tmp_file=tmp_file)
    logging.info("cmd:{}".format(cmd))
    run_cmd(cmd)

    mysql_host, mysql_port, mysql_user, mysql_password, mysql_database = get_mysql_config(
        config_file)

    mysql_import = '''mysql --user={mysql_user} --password='{mysql_password}'
    --host={host} --port={port} {mysql_database} < {tmp_file}'''.format(
        mysql_user=mysql_user,
        mysql_password=mysql_password,
        mysql_database=mysql_database,
        port=mysql_port,
        host=mysql_host,
        tmp_file=tmp_file).replace('\n', ' ')

    run_file(mysql_import, os.path.dirname(config_file))

    if os.path.exists(tmp_file):
        os.remove(tmp_file)


def update_game(game_dir):
    cmd = 'rsync -avP --password-file=/etc/rsync_user.secrets --exclude=m.sh --exclude=game.properties rsync_user@rsync.hundredcent.com::rsync_home/{product}/jar/ .'.format(product=product)
    logging.info("working_dir:{} cmd:{}".format(game_dir, cmd))
    run_file(cmd, game_dir)


def md5_check(game_dir):
    cmd = '/usr/bin/md5sum -c md5.txt'
    sarge_cmd(cmd, cwd=game_dir)


def update_hotfix(game_dir):
    today = time.strftime("%Y%m%d")
    cmd = '/bin/rm -rf hotfix/* && rsync -avP --password-file=/etc/rsync_user.secrets rsync_user@rsync.hundredcent.com::rsync_home/{product}/hotfix/{today}/ hotfix'.format(product=product, today=today)
    logging.info("working_dir:{} cmd:{}".format(game_dir, cmd))
    run_file(cmd, game_dir)


def get_md5(config_file):
    dir_name = os.path.dirname(config_file)
    jar = '{}_game.jar'.format(product.replace('-en', ''))
    jar_path = os.path.join(dir_name, jar)
    jar_md5 = md5(jar_path)

    return jar_md5


def process(config_file):
    mysql_host, mysql_port, mysql_user, mysql_password, mysql_database = get_mysql_config(
        config_file)

    conn = pymysql.connect(
        host=mysql_host,
        user=mysql_user,
        passwd=mysql_password,
        database=mysql_database,
        charset='utf8',
        use_unicode=True,
    )
    cursor = conn.cursor()

    sql_data = []

    for sql in sql_list:
        cursor.execute(sql)
        row = cursor.fetchone()
        sql_data.append(row[0])

    conn.close()
    tcp_port, http_port, server_id, server_name = sql_data
    game_dir = os.path.dirname(config_file)
    id_name = "{0}_{1}_{2}".format(product, server_id, server_name)
    _, stdout, _ = run_cmd("du -shc {}".format(game_dir))
    size = stdout.splitlines()[-1].split('\t')[0]
    md5sum = get_md5(config_file)
    proc_status, port_status = check_game(config_file, tcp_port)
    json_data = {
        'game_dir': os.path.split(game_dir)[-1],
        'mysql_host': mysql_host,
        'mysql_password': mysql_password,
        'mysql_database': mysql_database,
        'tcp_port': int(tcp_port),
        'http_port': int(http_port),
        'id_name': id_name,
        'size': size,
        'md5sum': md5sum,
        'proc_status': proc_status,
        'port_status': port_status,

    }

    check_result = check_data(id_name)

    update_sql_flag = check_result.get('update_sql', 0)
    update_game_flag = check_result.get('update_game', 0)
    update_hotfix_flag = check_result.get('update_hotfix', 0)
    server_status_flag = check_result.get('server_status', 'disable')
    game_status = check_result.get('game_status', '')

    if debug:
        server_status_flag = 'enable'

    if server_status_flag == 'enable':
        pass
    else:
        logging.error("config_file:{} server_status:{} or no record".format(config_file, server_status_flag))
        return

    if action == 'get_info':
        print(json.dumps(json_data, ensure_ascii=False))
    elif action == 'stop_game':
        if debug or game_status == 'stop':
            sarge_cmd('bash m.sh stop'.format(game_dir), cwd=game_dir)
    elif action == 'update_sql':
        if debug or update_sql_flag:
            update_sql(config_file)
    elif action == 'update_game':
        if debug or update_game_flag:
            update_game(game_dir)
    elif action == 'is_game_stop':
        proc_status, port_status = check_game(config_file, tcp_port)
        if proc_status == 0 and port_status == 0:
            logging.info("aciton:{0} dir:{1}:已停止 端口:{2} 进程状态:{3} 端口状态:{4}".format(action, os.path.dirname(config_file), tcp_port, proc_status, port_status))
        else:
            logging.error("aciton:{0} dir:{1}:未停止 端口:{2} 进程状态:{3} 端口状态:{4}".format(action, os.path.dirname(config_file), tcp_port, proc_status, port_status))

    elif action == 'is_game_start':
        if proc_status == 1 and port_status == 1:
            logging.info("aciton:{0} dir:{1}:已启动 端口:{2} 进程状态:{3} 端口状态:{4}".format(action, os.path.dirname(config_file), tcp_port, proc_status, port_status))
        else:
            logging.error("aciton:{0} dir:{1}:未启动 端口:{2} 进程状态:{3} 端口状态:{4}".format(action, os.path.dirname(config_file), tcp_port, proc_status, port_status))

    elif action == 'start_game':
        if debug or game_status == 'start':
            sarge_cmd('bash m.sh start', cwd=game_dir)
    elif action == 'md5_check':
        md5_check(game_dir)
    elif action == 'update_hotfix':
        if debug or update_hotfix_flag:
            update_hotfix(game_dir)
    else:
        logging.error("config_file:{} action:{} is not in choices".format(config_file, action))


path_list = get_config_path()

for config_file in path_list:
    base_dir_name = os.path.basename(os.path.dirname(config_file))
    if assign_dir == "all" or base_dir_name in assign_dir_list:
        process(config_file)
