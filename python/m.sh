#!/bin/sh

PRO_DIR=`dirname $0`;
PRO_DIR=`cd $PRO_DIR/;pwd`;
TIME_SUFFIX=`date +%Y%m%d%H`;
PWD_PATH=`pwd | awk -F/ '{print $NF}'`

echo $PRO_DIR

case $1 in
	start)
		pid=`cat ./pid`
		process=`ps -ef | grep "$PWD_PATH/honor_game.jar" | grep -v "grep" | wc -l`;
		if [ "$process" -eq 1 ]; then
			echo -e "\033[32;31;1;2m game server already started!! \033[m";
		else
			nohup java -Xms2048m -Xmx8450m -javaagent:honor_game_lib/game-agent.jar -Djava.ext.dirs=honor_game_lib/ -jar $PRO_DIR/honor_game.jar >> /dev/null 2>>error.log &
			echo $! > pid;
			echo -e "\033[32;31;1;2m start game server success!! \033[m";
			exit;
		fi
		;;

	restart)
		process=`ps -ef | grep "$PWD_PATH/honor_game.jar" | grep -v "grep"`;
		echo $process;
		if [ "$process" == "" ]; then
			./run.sh start;
			exit;
		else
			./run.sh stop;
		fi

		while true
		do
			process=`ps -ef | grep "$PWD_PATH/honor_game.jar" | grep -v "grep"`;
			if [ "$process" == "" ]; then
				./run.sh start;
			break;
			else
				sleep 1;
				echo "process exsits"
			fi
		done
		;;

	stop)
		stop_pid=`ps -ef | grep "$PWD_PATH/honor_game.jar" | grep -v "grep" |awk '{print $2}'`
		kill $stop_pid;
		echo -e "\033[32;31;1;2m stop game server success!! \033[m";
		;;

	check)
		ps -ef | grep "$PWD_PATH/honor_game.jar" | grep -v "grep";
		;;

	back)
		rm -rf honor_game.jar.bak;
		mv honor_game.jar honor_game.jar.bak;
		;;

esac
