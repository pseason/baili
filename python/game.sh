source /etc/profile

start() {
    /usr/bin/python2.7 /opt/tools/update_agent.py --debug=True --action=start_game
}

stop() {
    /usr/bin/python2.7 /opt/tools/update_agent.py --debug=True --action=stop_game
}

status() {
    /usr/bin/python2.7 /opt/tools/update_agent.py --debug=True --action=is_game_start
}

get_info() {
    /usr/bin/python2.7 /opt/tools/update_agent.py --debug=True --action=get_info
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    status)
        status
        ;;
    get_info)
        get_info
        ;;
    *)
        echo $"Usage: $0 {start|stop|status|get_info}"
        exit 2
esac
