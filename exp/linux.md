# linux command

### 清除缓存
```sh
 echo > 3 /proc/sys/vm/drop_caches
```

### cat
```sh
cat `find /opt/backup/log/zombie_*/2018-11-*/game_*.log` |grep 'onlineTime' > onlineTime.log
```

### tar
```sh
# 压缩
tar -czvf  xxx.tar.gz file/dir
# 查看所有内容
zcat xxx.tar.gz
# 查看压缩文件目录
tar -xvf xxx.tar.gz
# 压　缩：
tar -jcv -f filename.tar.bz2 要被压缩的文件或目录名称
# 查　询：
tar -jtv -f filename.tar.bz2
# 解压缩：
tar -jxv -f filename.tar.bz2 -C 欲解压缩的目录
# 解压缩
tar -zxvf filename.gz.tar
```

### rpm
```sh
# 安装并在安装过程中显示正在安装的文件信息及安装进度(i 安装 v 视图 h 进度)
rpm -ivh  xxx.rpm
# 卸载
rpm -e jenkins
```

### nc 使用nc命令监视端口
```sh
nc -l 9000
```

### 复制远程文件
```sh
从主机复制下来
scp -P 8022 root@10.104.46.126:/opt/www/server/honor_merge_105/log/game.log.2019-01-01 /opt/www/server/
复制到主机
scp -P 8022 glory_account.war root@172.16.0.8:/opt/www/server
```

### 行数
```bash
cat register.20190326.log | awk 'BEGIN{i=0}{i++;print i}'
```
