# mysql

### 查询myusql资源全局变量
```MYSQL
 show global variables like '%secure%';
```

### mysql （load data local infile）导入文本数据到myssql
```sh
LOAD DATA LOCAL INFILE 'C:/ProgramData/MySQL/MySQL Server 5.6/Uploads/student.log' INTO TABLE student fields terminated by '|';

or

LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 5.6/Uploads/student.log' INTO TABLE student fields terminated by '|';
```

```sh
/opt/mysql/bin/mysql -h10.66.223.91 -uroot -pjeC02GfP bgjs_gm -e "LOAD DATA LOCAL INFILE 'login.log' IGNORE INTO TABLE r_login fields terminated by'|'"
/opt/mysql/bin/mysql -h10.66.223.91 -uroot -pjeC02GfP bgjs_gm -e "LOAD DATA LOCAL INFILE 'register.log' IGNORE INTO TABLE r_register fields terminated by'|'"
/opt/mysql/bin/mysql -h10.66.223.91 -uroot -pjeC02GfP bgjs_gm -e "LOAD DATA LOCAL INFILE 'pay.log' IGNORE INTO TABLE r_pay fields terminated by'|'"
```

### 导入文件
```sh
mysql -h10.66.197.134 -uroot -pjeC02GfP behavior_log201811 -e "LOAD DATA LOCAL INFILE 'medal.log' IGNORE INTO TABLE r_medal_20181101 fields terminated by'|'  ENCLOSED BY '\"' "
```
### 导入文件
```sh
 mysql -h10.66.197.134 -uroot -pjeC02GfP honor_gm < u_action_point_task.sql
```

### 复制表结构和数据
```MySQL
create table p_smallid_copy like p_smallid;
insert into p_smallid_copy select * from p_smallid;
```

### 创建索引
```mysql
create unique index xxx_name on table_name(column1,column2)
```
