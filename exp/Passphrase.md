# linux 安装Passphrase key 访问

### 步骤
```step
1. 访问机器 ssh-keygen -t rsa （一键回车）
   生成文件目录 /root/.ssh/
2. 将本地id_rsa.pub文件的内容拷贝至远程服务器的~/.ssh/authorized_keys文件中
3.
```

### 注释
```note
authorized_keys:存放远程免密登录的公钥,主要通过这个文件记录多台机器的公钥
id_rsa : 生成的私钥文件
id_rsa.pub ： 生成的公钥文件
know_hosts : 已知的主机公钥清单
如果希望ssh公钥生效需满足至少下面两个条件：
  1) .ssh目录的权限必须是700
  2) .ssh/authorized_keys文件权限必须是600
```
