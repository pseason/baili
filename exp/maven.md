# maven

### 指定maven home
```sh
vi /etc/profile
#位置
unset i
unset -f pathmunge
export MAVEN_HOME=/opt/apache-maven-3.6.0
export PATH=:$PATH:$MAVEN_HOME/bin
```

### profile 生效
```sh
source /etc/profile
```
