# jenkins



### 安装
```sh
rpm -ivh jenkins-2.150.1-1.1.noarch.rpm
whereis jenkins
```

### jenkins端口
```sh
vi /etc/sysconfig/jenkins
# 启动参数
JENKINS_JAVA_OPTIONS="-XX:MaxPermSize=512m -Djava.awt.headless=true"
```

### jenkins jdk路径指定
```sh
vim /etc/init.d/jenkins
```

### jenkins log dir
```sh
/var/log/jenkins/jenkins.log
```

### jenkins config dir
```sh
cd /var/lib/jenkins
cd /var/cache/jenkins/war
cat /var/lib/jenkins/secrets/initialAdminPassword
```

### jenkins 提示离线解决办法
```sh
# www.google.com --> www.baidu.com
vi /var/lib/jenkins/updates/default.json
# https --> http
vi /var/lib/jenkins/hudson.model.UpdateCenter.xml
```

### 启动jenkins
```sh
service jenkins start
service jenkins restart
service jenkins stop
```

### 登录地址
```http
http://xxx:xxx/login?from=%2F
```

### jenkins 插件
```text
  语言包：Local   ---> zh_CN
```
