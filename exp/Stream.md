# java lambda stream

### for
```java
Stream.iterate(1,i->i+1 ).limit(10).forEach(System.out::println);
```

### joining
```java
Arrays.asList(1,2,3,4).stream().map(i -> i.toString()).collect(Collectors.joining("-"));
```

### group by (one group by)
```java
Map<String,List<PlatformStats>> map =
    maplist.stream()
    .collect(Collectors.groupingBy(PlatformStats::getDate));
```

### group by (two group by)
```java
Map<String,Map<String,List<PlatformStats>>> map =
    src.stream().collect(Collectors.groupingBy(
      PlatformStats::getDate,Collectors.groupingBy(PlatformStats::getPlatNo)
    ));
```
