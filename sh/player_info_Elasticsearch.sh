#!/bin/sh
. /etc/profile

#  玩家行为日志入库

  
Path='/opt/transfer/log/transfer_tank_account/game/2018-07-03/';
cd $Path;
game_names=`ls|grep "[0-9]"`;
array=(
    "equip"
    "part"
    "partMatrial"
    "medal"
    "awakenHeros"  
    "militaryMaterial"
    "lordEquip"
    "lordEquipMaterial"
    "tank"
    "energy" 
    "militaryScience"
    "prop"
    "lordInfo"
    "labInfoPbTwoInt"
    "labGraduateInfo"                                          
    );
arr=();

date '+%Y-%m-%d %H:%M:%S'
for tank in $game_names
do
    logPath="$Path$tank";
    cd $logPath
    pwd
    for(( i=0;i<${#array[@]};i++))
    do
    	behavior=${array[i]};
        #文件
        myFile="$logPath/$behavior.log";
        echo $myFile;
        #导入数据
         curl  -s -XPOST "10.135.102.102:9200/_bulk?pretty" -H "Content-Type: application/json" --data-binary @$myFile >/dev/null 2>&1
    done
done
date '+%Y-%m-%d %H:%M:%S'

