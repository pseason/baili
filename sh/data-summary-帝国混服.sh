#!/bin/sh
#数据汇总


mysql_host="rm-bp1jar1yblgdu40um.mysql.rds.aliyuncs.com";
mysql_password="jeC02GfP";
mysql_user="baili"
#源数据库
mysql_datebses="empire_hf";
#目标数据库
target_mysql_datebses="empire_data_sum_hf"
account_mysql_datebses="empire_account_hf"

if [ $1 ]; then
    #有传参
   fileday=$1;
else
  #没有 传参
   fileday=`date -d last-day +%Y-%m-%d`;
fi







#账号总和
#sum_account
 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE INTO ${target_mysql_datebses}.sum_account
SELECT  date(createDate)  as register_date , platNo,count(DISTINCT platId) as sum_platId,count(DISTINCT deviceNo) as deviceNo,childNo  from ${account_mysql_datebses}.u_account where
createDate  >= '$fileday' GROUP BY date(createDate),platNo,childNo"






#登陆sum_login
 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_login
SELECT  a.date  as  login_date,a.serverId,a.platNo,sum_lordId,new_sum_lordId,a.childNo from (
SELECT serverId,platNo,childNo,date(loginLast) as date , count(DISTINCT lordId) as sum_lordId  FROM r_login
where  loginLast >= '$fileday'  GROUP BY serverId,platNo,childNo,date(loginLast)
) as a LEFT JOIN(
  SELECT serverId,platNo,childNo,date(createDate) as date , count(DISTINCT lordId) as new_sum_lordId FROM r_login
  where  createDate >= '$fileday'   GROUP BY serverId,platNo,childNo,date(createDate)
) as b on a.serverId=b.serverId and b.platNo=a.platNo  and b.childNo=a.childNo   and a.date =b.date"


#充值sum_pay
#删除记录
time mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"DELETE from ${target_mysql_datebses}.sum_pay where pay_date >='$fileday';"

 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_pay
SELECT pay_date,a.serverId,a.platNo,a.sum_lordId,new_lordId_sum,pay_number,sum_amount,new_amount_sum,a.childNo  from (
  SELECT serverId,platNo,childNo,date(payTime) as pay_date , count(DISTINCT roleId) as sum_lordId,count(*) as pay_number ,sum(amount) as sum_amount,count(platId) as platId_sum FROM ${account_mysql_datebses}.u_pay
  where state=1  and  payTime >= '$fileday'  GROUP BY serverId,platNo,childNo,date(payTime)
)  as a LEFT JOIN (
  SELECT q.serverId,count(DISTINCT q.roleId) as new_lordId_sum,platNo,childNo,payTime,sum(amount) as new_amount_sum from (
    SELECT serverId,platNo,childNo,roleId,date(payTime) as payTime ,sum(amount) as amount  FROM ${account_mysql_datebses}.u_pay
    where state=1  and   payTime >= '$fileday'  GROUP BY serverId,roleId,payTime
  ) as q inner JOIN (
    SELECT DISTINCT serverId,lordId,date(registerDate) as registerDate FROM r_register
    where  registerDate >= '$fileday'  and created=1
  ) as  w on q.serverId=w.serverId and q.roleId=w.lordId and w.registerDate=q.payTime  GROUP BY q.serverId,platNo,childNo,date(payTime)
) as c on c.serverId=a.serverId and a.platNo=c.platNo and a.childNo=c.childNo and c.payTime=a.pay_date"


#充值注册角色
#sum_lordid_register_pay

 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_lordid_register_pay
SELECT reg_date as register_date,paydate as  pay_date,c.serverId,c.platNo,sum(amount) as sum_amount ,count( DISTINCT c.roleId) as sum_lordId,c.childNo  from (
    SELECT serverId,roleId,date(payTime) as  paydate,amount,platNo,childNo from ${account_mysql_datebses}.u_pay  where state=1  and  payTime >= '$fileday'
) as c LEFT  JOIN(
    SELECT date(registerDate) as reg_date,lordId,serverId,platNo,childNo from r_register  where created=1
    and  lordId in( SELECT roleId from ${account_mysql_datebses}.u_pay  where state=1  and payTime >= '$fileday')
    GROUP BY serverId,lordId
) as q on c.serverId=q.serverId and c.roleId=q.lordId  where reg_date is NOT null  GROUP BY paydate,c.serverId,reg_date,c.platNo,c.childNo"








#角色sum_register
 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_register
SELECT date(registerDate) as register_date , serverId,platNo,count(DISTINCT lordId) as sum_lordId,count(DISTINCT deviceNo) as sum_deviceNo,childNo  FROM r_register
where  registerDate >= '$fileday'  and created=1 GROUP BY serverId,platNo,childNo,date(registerDate)"




#注册分角色登陆
#sum_lordid_register_login
 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_lordid_register_login
SELECT date(createDate) as  register_date,serverId,platNo,count(DISTINCT lordId) as lordId_sum,date(loginLast) as login_date,childNo  FROM r_login
where  loginLast >= '$fileday' GROUP BY serverId,platNo,childNo,date(loginLast),date(createDate)"



# #首冲玩家时间
# #sum_reg_pay_lordId
 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_reg_pay_lordId
SELECT
  date(createDate) as reg_date,
  pay_date,
  reg.serverId,
  platNo,
  count(DISTINCT lordId) as pay_lordId,childNo
FROM
  r_register AS reg
INNER JOIN (
SELECT roleId,serverId,date(min(payTime))  as pay_date from ${account_mysql_datebses}.u_pay
where
(roleId,serverId)in(SELECT DISTINCT roleId,serverId FROM ${account_mysql_datebses}.u_pay WHERE date(payTime) = '$fileday'  AND state = 1) GROUP BY roleId,serverId
) AS c ON reg.serverId = c.serverId
AND reg.lordId = c.roleId
AND reg.created = 1
where pay_date = '$fileday'
GROUP BY pay_date,reg.serverId,reg.platNo,childNo,reg_date
"





# #账号注册登陆
# #sum_platid_register_login


 mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE INTO ${target_mysql_datebses}.sum_platid_register_login
SELECT date(createDate) as  register_date,platNo,count(DISTINCT platId) as platId_sum,date(loginLast) as login_date,childNo  FROM r_login
where  loginLast >= '$fileday'  GROUP BY platNo,childNo,date(loginLast),date(createDate)"





# #1,7,15,30,60,90留存
# #sum_keep
 mysql -h$mysql_host -u$mysql_user -p$mysql_password   $target_mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_keep
SELECT L0.date,L0.serverId,L0.platNo,L1,L3,L7,L15,L30,L60,L90,L0.childNo from (
  SELECT register_date as date ,sum(lordId_sum) as L0,serverId,platNo,childNo  from sum_lordid_register_login where
  register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
 GROUP BY date,serverId,platNo,childNo
  ) as L0
  LEFT JOIN(

    SELECT register_date as date ,sum(lordId_sum) as L1,serverId,platNo,childNo  from sum_lordid_register_login  where
    to_days(login_date) = to_days(register_date)+1
   and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
    GROUP BY date,serverId,platNo,childNo

)as L1 on L0.serverId=L1.serverId and L0.platNo=L1.platNo and L0.childNo=L1.childNo  and L0.date =L1.date
LEFT JOIN(


    SELECT register_date as date ,sum(lordId_sum) as L3,serverId,platNo,childNo  from sum_lordid_register_login  where
    to_days(login_date) = to_days(register_date)+3
   and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
    GROUP BY date,serverId,platNo,childNo



) as L3 on L0.serverId=L3.serverId and L0.platNo=L3.platNo and L0.childNo=L3.childNo and L0.date =L3.date LEFT JOIN(



    SELECT register_date as date ,sum(lordId_sum) as L7,serverId,platNo,childNo  from sum_lordid_register_login  where
    to_days(login_date) = to_days(register_date)+7
   and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
    GROUP BY date,serverId,platNo,childNo


) as L7 on L0.serverId=L7.serverId and L0.platNo=L7.platNo and L0.childNo=L7.childNo  and L0.date =L7.date
LEFT JOIN(


  SELECT register_date as date ,sum(lordId_sum) as L15,serverId,platNo,childNo  from sum_lordid_register_login  where
    to_days(login_date) = to_days(register_date)+15
   and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
    GROUP BY date,serverId,platNo,childNo



) as L15 on L0.serverId=L15.serverId and L0.platNo=L15.platNo and L0.childNo=L15.childNo and L0.date =L15.date
LEFT JOIN(


  SELECT register_date as date ,sum(lordId_sum) as L30,serverId,platNo,childNo  from sum_lordid_register_login  where
    to_days(login_date) = to_days(register_date)+30
   and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
    GROUP BY date,serverId,platNo,childNo

) as L30 on L0.serverId=L30.serverId and L0.platNo=L30.platNo and L0.childNo=L30.childNo and L0.date =L30.date
LEFT JOIN(

  SELECT register_date as date ,sum(lordId_sum) as L60,serverId,platNo,childNo  from sum_lordid_register_login  where
    to_days(login_date) = to_days(register_date)+60
   and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
    GROUP BY date,serverId,platNo,childNo


) as L60 on L0.serverId=L60.serverId and L0.platNo=L60.platNo and L0.childNo=L60.childNo and L0.date =L60.date
LEFT JOIN(

  SELECT register_date as date ,sum(lordId_sum) as L90,serverId,platNo,childNo  from sum_lordid_register_login  where
    to_days(login_date) = to_days(register_date)+90
   and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()
    GROUP BY date,serverId,platNo,childNo

) as L90 on L0.serverId=L90.serverId and L0.platNo=L90.platNo and L0.childNo=L90.childNo and L0.date =L90.date
"




# 角色信息表

 mysql -h$mysql_host -u$mysql_user -p$mysql_password   $mysql_datebses -e"REPLACE into ${mysql_datebses}.r_role
  	SELECT loginLast,r.lordId,nick,vip,topup,prosMax,gold,goldCost,goldGive,accountKey,r.serverId,level,platNo,platId,deviceNo,createDate,amount,payTime,childNo  FROM(
		SELECT * from ${mysql_datebses}.r_login where loginLast >='$fileday'
	)   as r   left JOIN(
		SELECT serverId,roleId,sum(amount) as amount,max(payTime) as payTime from ${account_mysql_datebses}.u_pay  GROUP BY serverId,roleId

	) as p ON r.lordId =p.roleId and r.serverId=p.serverId"


#金币持有量
 mysql -h$mysql_host -u$mysql_user -p$mysql_password   $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_gold_holdings
SELECT c.serverId,count_lordId ,sum_gold ,active_gold,active_lordid
 from (SELECT serverId,count(lordId) as count_lordId,SUM(gold) as sum_gold from r_role GROUP BY serverId) as c
LEFT JOIN (
SELECT sum(gold) AS active_gold,COUNT(lordId) AS active_lordid,serverId FROM r_role a WHERE
  lordId IN (SELECT lordId FROM r_role as c WHERE c.serverId = a.serverId AND loginLast > date_sub(curdate(), INTERVAL 30 DAY))
    AND loginLast > date_sub(curdate(), INTERVAL 30 DAY) GROUP BY serverId
)  as q  on  q.serverId = c.serverId "

#充值账号分渠道
#${target_mysql_datebses}.sum_u_pay
  mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_u_pay
 SELECT c.serverId,c.roleId,platId,platNo,payTime,amount,orderId,serialId,state,addGold,childNo from (
  select serverId,roleId,amount,payTime,state,orderId,serialId,addGold from  ${account_mysql_datebses}.u_pay where
          payTime >='$fileday'
) as c LEFT JOIN(
  select serverId,lordId,platId,platNo,childNo from  ${mysql_datebses}.r_role
) as q on c.serverId=q.serverId and c.roleId=q.lordId"





#充值账号分渠道
#sum_account_pay
  mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_account_pay
 SELECT reg_date as register_date,paydate as  pay_date,c.platNo,sum(amount) as sum_amount ,count( DISTINCT q.platId) as sum_platId,c.childNo  from (
     SELECT  date(payTime) as  paydate,amount,platNo,childNo,platId from ${target_mysql_datebses}.sum_u_pay  where state=1  and  payTime >= '$fileday'
 ) as c LEFT  JOIN(
     SELECT date(createDate) as reg_date,platId,platNo,childNo from ${account_mysql_datebses}.u_account  where
   platId in( SELECT DISTINCT platId from ${target_mysql_datebses}.sum_u_pay  where state=1  and payTime >= '$fileday' )
 ) as q  on  c.platId=q.platId  and  c.platNo=q.platNo  and  c.childNo=q.childNo  where reg_date is NOT null  GROUP BY paydate,reg_date,q.platNo,q.childNo"


#充值留存缓存
#sum_lordid_register_pay_login 充值留存
  mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_lordid_register_pay_login
SELECT date(createDate),serverId,platNo,count(1) as lordId_sum ,date(loginLast),childNo from (
  SELECT createDate,serverId,platNo,lordId,loginLast,childNo FROM $mysql_datebses.r_login WHERE
  loginLast>='$fileday'
  ) as cc where
   (serverId, lordId) IN (
   SELECT
            serverId,
            lordId
          FROM
            ${mysql_datebses}.r_role
          WHERE
            (
              serverId,
              lordId,
              date(createDate)
            ) IN (
      SELECT  serverId,roleId,date(payTime) FROM ${account_mysql_datebses}.u_pay GROUP BY serverId,roleId,date(payTime)
  )
) GROUP BY date(createDate),serverId,platNo,childNo,date(loginLast)"






#充值留存缓存1,3,7,15,30,90
 mysql -h$mysql_host -u$mysql_user -p$mysql_password   $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_pay_keep
  SELECT a0.date,a0.serverId,a0.platNo, l1,l3,l7,l15,l30,l60,l90,a0.childNo from (

  SELECT  date(register_date) as date ,serverId,platNo,childNo from ${target_mysql_datebses}.sum_lordid_register_pay_login where  register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

)as a0  LEFT JOIN (
  SELECT  date(register_date) as date ,serverId,platNo,childNo,sum(lordId_sum) as l1 from ${target_mysql_datebses}.sum_lordid_register_pay_login where   to_days(login_date) = to_days(register_date) + 1  and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

) as a1 on a1.serverId=a0.serverId and a1.platNo=a0.platNo and a1.childNo=a0.childNo and a1.date=a0.date LEFT JOIN (

   SELECT  date(register_date) as date ,serverId,platNo,childNo,sum(lordId_sum) as l3 from ${target_mysql_datebses}.sum_lordid_register_pay_login where   to_days(login_date) = to_days(register_date) + 3  and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

)as  a3 on a3.serverId=a0.serverId and a3.platNo=a0.platNo and a3.childNo=a0.childNo  and a3.date=a0.date
LEFT JOIN (

   SELECT  date(register_date) as date ,serverId,platNo,childNo,sum(lordId_sum) as l7 from ${target_mysql_datebses}.sum_lordid_register_pay_login where   to_days(login_date) = to_days(register_date) + 7  and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

)as  a7 on a7.serverId=a0.serverId and a7.platNo=a0.platNo and a7.childNo=a0.childNo  and a7.date=a0.date
LEFT JOIN (

  SELECT  date(register_date) as date ,serverId,platNo,childNo,sum(lordId_sum) as l15 from ${target_mysql_datebses}.sum_lordid_register_pay_login where   to_days(login_date) = to_days(register_date) + 15  and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

)as  a15 on a15.serverId=a0.serverId and a15.platNo=a0.platNo and a15.childNo=a0.childNo and a15.date=a0.date
LEFT JOIN (

   SELECT  date(register_date) as date ,serverId,platNo,childNo,sum(lordId_sum) as l30 from ${target_mysql_datebses}.sum_lordid_register_pay_login where   to_days(login_date) = to_days(register_date) + 30  and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

)as  a30 on a30.serverId=a0.serverId and a30.platNo=a0.platNo and a30.childNo=a0.childNo and a30.date=a0.date
LEFT JOIN (

   SELECT  date(register_date) as date ,serverId,platNo,childNo,sum(lordId_sum) as l60 from ${target_mysql_datebses}.sum_lordid_register_pay_login where   to_days(login_date) = to_days(register_date) + 60  and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

)as  a60 on a60.serverId=a0.serverId and a60.platNo=a0.platNo and a60.childNo=a0.childNo  and a60.date=a0.date
LEFT JOIN (

   SELECT  date(register_date) as date ,serverId,platNo,childNo,sum(lordId_sum) as l90 from ${target_mysql_datebses}.sum_lordid_register_pay_login where   to_days(login_date) = to_days(register_date) + 90  and register_date BETWEEN date_sub(curdate(), INTERVAL 100 DAY) and curdate()  GROUP BY
  date(register_date),serverId,platNo,childNo

)as  a90 on a90.serverId=a0.serverId and a90.platNo=a0.platNo and a90.childNo=a0.childNo and a90.date=a0.date;"



#等级分布
#sum_level
 mysql -h$mysql_host -u$mysql_user -p$mysql_password   $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_level
SELECT '$fileday' as date , \`level\`,serverId,platNo,count(lordId),childNo from ${mysql_datebses}.r_role where platNo IS not null   GROUP BY \`level\`,serverId,platNo,childNo
"



#充值等级分布
#sum_pay_level
 mysql -h$mysql_host -u$mysql_user -p$mysql_password   $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_pay_level
 SELECT date,c.serverId,c.platNo,\`level\`,sum(c.amount) AS amount,sum(pay_count) AS pay_count,count(roleId) AS role_count,c.childNo
 FROM
  (
    SELECT serverId,roleId,platNo,sum(amount) AS amount,count(1) AS pay_count,date(payTime) AS date ,childNo
    FROM
      ${account_mysql_datebses}.u_pay
    WHERE
      state = 1
    GROUP BY
      serverId,
      roleId,
      date(payTime)
  ) AS c
LEFT JOIN ${mysql_datebses}.r_role AS a ON c.serverId = a.serverId AND a.lordId = c.roleId
GROUP BY
  date,
  \`level\`,
  platNo,
  childNo,
  serverId
"



#首冲时间分布
#sum_first_pay
mysql -h$mysql_host -u$mysql_user -p$mysql_password  $mysql_datebses -e"REPLACE into ${target_mysql_datebses}.sum_first_pay
SELECT
  c.*, m.os FROM(
    SELECT date,serverId,platNo,count(roleId) AS pay_count,amount,childNo FROM (
        SELECT serverId,platNo,childNo,roleId,date(min(payTime)) AS date,amount FROM ${account_mysql_datebses}.u_pay
        where payTime >='$fileday'
         GROUP BY serverId,roleId
      ) c
    GROUP BY date,amount,platNo,serverId,childNo
  ) AS c
LEFT JOIN ${mysql_datebses}.m_channel AS m ON c.platNo = m.channelId and c.childNo = m.childNo"
