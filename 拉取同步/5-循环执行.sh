#!/bin/bash
. /etc/profile

echo "------------------------------"
date '+%Y-%m-%d %H:%M:%S'
array={
  "2018-11-16 08"
}
for day in "${array[@]}"
do
    #拉取数据
    python get.py -d$arg;
    #存取数据
    bash data.sh "${day}";
done
date '+%Y-%m-%d %H:%M:%S'
echo "------------------------------"
