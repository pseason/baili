# u_pay

## 查询支付流水

### 查询某个平台范围时间内支付流水
```mysql
select *
from u_pay
where payTime > '2018-08-01'
  and payTime < '2018-09-01'
  and platNo = 311
  and childNo = 0
order by payTime desc;
```

### 统计某个平台范围时间内支付总额
```mysql
select month(payTime) as month,platNo, childNo, sum(amount) as sum_amount
from u_pay
where payTime > '2018-08-01'
  and payTime < '2018-09-01'
  and platNo = 311
  and childNo = 0
group by platNo,childNo
```
