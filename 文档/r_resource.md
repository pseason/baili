# r_resource && m_behavior && m_pop

## 查询资源消耗

### 查询 时间范围内某个玩家获取资源数量
```mysql
区服  玩家ID  玩家昵称  获取资源
SELECT
	serverId,
	lordId,
	nick,
	sum(increase)
FROM
	r_resource_20181013
WHERE
	getCode IN (192, 193)
AND createDate >= '2018-10-20 00:00:00'
AND createDate <= '2018-10-20 00:30:00'
AND id = 4
GROUP BY
	serverId,
	lordId
ORDER BY
	serverId,
	lordId;
```
