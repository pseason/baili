# r_login

## 查询登录信息

### 查询 单设备登录信息
```mysql
登录日期  区服编号  设备编号   单设备登录次数
SELECT
			date(loginLast) AS date,
			serverId,
			deviceNo,
			count(DISTINCT lordId) AS sum_lordId
		FROM
			r_login
		WHERE
			serverId = '619' and loginLast <= '2018-10-15 00:00:00' and loginLast >= '2018-09-13 00:00:00'
		GROUP BY
			serverId,
			deviceNo,
			date(loginLast)
		order date(loginLast) desc;
```

### 玩家等级分布情况
```mysql
select serverId,
CASE
	WHEN level between 0 and 10 THEN '0~10'
	WHEN level between 11 and 20 THEN '11~20'
	WHEN level between 21 and 30 THEN '21~30'
	WHEN level between 31 and 40 THEN '31~40'
	WHEN level between 41 and 50 THEN '41~50'
	WHEN level between 51 and 60 THEN '51~60'
	WHEN level between 61 and 70 THEN '61~70'
	WHEN level between 71 and 80 THEN '71~80'
	WHEN level between 81 and 90 THEN '81~90'
	WHEN level between 91 and 100 THEN '91~100'
	WHEN level between 101 and 110 THEN '101~110'
END as lv_ ,
count(distinct lordId) from r_login_ where loginLast >= '2018-10-24 00:00:00' group by serverId,lv_
order by serverId,lv_
;
```
