# u-pay和sum-pay的差别

### u-pay和sum-pay的差别
```mysql
select a.*,(a.amount-b.sum_amount) aa from (
	select DATE_FORMAT(payTime,'%Y-%m-%d') payTime,serverId,platNo,childNo,sum(amount) amount from empire_account_hf.u_pay
	where payTime between '2018-08-01' and '2018-09-01' and state = 1
	group by DATE_FORMAT(payTime,'%Y-%m-%d'),serverId,platNo,childNo
	order by payTime, serverId,platNo,childNo
) a
,
(
	select * from sum_pay
	where pay_date between '2018-08-01' and '2018-08-31'
	order by pay_date,serverId,platNo,childNo
) b
where a.payTime = b.pay_date and a.serverId = b.serverId and a.platNo = b.platNo and a.childNo = b.childNo
having aa != 0;
```
