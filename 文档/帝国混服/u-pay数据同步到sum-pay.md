# u-pay数据同步到sum-pay

### 同步数据 (注意同步使用 realAmount 金额 state = 1)
```mysql
set @startTime ='2018-08-01';
set @endTime   ='2018-09-01';
SELECT
	pay_date,
	a.serverId,
	a.platNo,
	a.sum_lordId,
	ifnull(new_lordId_sum,0) new_lordId_sum,
	ifnull(pay_number,0) pay_number,
	ifnull(sum_amount,0) sum_amount,
	ifnull(new_amount_sum,0) new_amount_sum,
	a.childNo  
from (
  SELECT serverId,platNo,childNo,date(payTime) as pay_date , count(DISTINCT roleId) as sum_lordId,
	count(*) as pay_number ,sum(amount) as sum_amount,count(platId) as platId_sum FROM empire_account_hf.u_pay
  where   payTime between @startTime and @endTime and state = 1 GROUP BY serverId,platNo,childNo,date(payTime) order by payTime
)  as a
LEFT JOIN (
  SELECT q.serverId,count(DISTINCT q.roleId) as new_lordId_sum,platNo,childNo,payTime,sum(amount) as new_amount_sum from (
    SELECT serverId,platNo,childNo,roleId,date(payTime) as payTime ,sum(amount) as amount  FROM empire_account_hf.u_pay
    where payTime between @startTime and @endTime and state = 1 GROUP BY serverId,roleId,payTime order by payTime
  ) as q inner JOIN (
    SELECT DISTINCT serverId,lordId,date(registerDate) as registerDate FROM empire_hf.r_register
    where  registerDate between @startTime and @endTime and created=1
  ) as  w
	on q.serverId=w.serverId and q.roleId=w.lordId and w.registerDate=q.payTime  GROUP BY q.serverId,platNo,childNo,date(payTime)
) as c
on c.serverId=a.serverId and a.platNo=c.platNo and a.childNo=c.childNo and c.payTime=a.pay_date
order by pay_date,serverId,platNo
```
