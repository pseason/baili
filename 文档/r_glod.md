# r_glod

## 查询金币情况

### 查询 时间范围内某个玩家进行某个操作金币情况
```mysql
区服  玩家ID  玩家昵称  金币消耗
SELECT
	serverId,
	lordId,
	nick,
	sum(add_gold)
FROM
	r_gold_20181001
WHERE
	getCode IN (192, 193)
AND createDate >= '2018-10-20 00:00:00'
AND createDate <= '2018-10-20 00:30:00'
GROUP BY serverId,lordId
ORDER BY
	serverId,
	lordId;
```
