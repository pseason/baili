#!/bin/sh
. /etc/profile

#  玩家行为日志入库
if [ -n "$1" ]; then
    #有传参
   fileday=$1; 
else  
  #没有 传参
   fileday=`date -d '-1 hour' +"%Y-%m-%d %H"`;
fi 
day=`date -d "${fileday}" +"%Y-%m-%d"`;

daydb=`date -d "${fileday}" +"%Y%m%d"`;
h=`date -d "${fileday}" +"%H"`;

Path='/opt/backup/log/';
cd $Path;


game_names=`ls|grep "zombie_[0-9]*[^.]"`;


mysql_password="jeC02GfP";
mysql_user="root"
mysql_datebses="player_behavior";
mysql_host="10.66.223.91";

array=(
    "gold"
    "activityProp"
    "activity"
    "logActivity"
    "prop"
    "medal"
    "medalBouns"
    "medalChip"
    "medalMaterial"
    "resource"
    "equip"
    "part"
    "chip"
    "partMaterial"
    "hero"
    "militaryMaterial"
    "arena"
    "frame"
    "science"
    "build"
    "tank"
    "contribution"
    "mail"
    "energyStone"
    "awakenHero"
    "command"
    "lordEquipMaterial"
    "lordEquip"
    "exploit"
    "monthSign"
    "militaryExploitChange"  
    "buyQuota"
    "playerChannelNum"
    "addnewhero"     
    "addHeroExpBook"     
    "addHeroChip"
    "logout"
    "onlineTime"  
    );

arr=();


date '+%Y-%m-%d %H:%M:%S'
for tank in $game_names
do
    logPath="${Path}${tank}/${day}";
    # echo "logPath:"$logPath;
    tmp="$logPath/tmp/${h}/";
    # echo "tmp:"$tmp;
    
    if [ ! -d "$logPath" ]; then
      mkdir -p "$logPath"
    fi
     cd $logPath
    if [ ! -d "$tmp" ]; then
      mkdir -p "$tmp"
    fi
   

    pwd
    rm -f $tmp*
    awk -F'|' '{print > "'$tmp'"$2.".log"}' "${logPath}/game_${h}.log"
    #导入数据  
    for(( i=0;i<${#array[@]};i++))
    do
        behavior=${array[i]};
        #文件
        myFile="${tmp}${behavior}.log";
        # echo $myFile;
        #导入数据
        if [ -f "$myFile" ]; then
            str="$(echo $behavior | tr '[:upper:]' '[:lower:]')";
            table_name="r_${str}";
            if [ ! ${arr[i]} ]; then
                databases=`curl "http://127.0.0.1:8081/zombie_gm/index.php?s=/api/BehaviorLog/points_table&time=${daydb}&table=${table_name}"`;
                echo "-------http://127.0.0.1:8081/zombie_gm/index.php?s=/api/BehaviorLog/points_table&time=${daydb}&table=${table_name}---------";
                arr[i]=$databases;
                echo "-----------------------数据库:${databases} ----------------"
            fi  
            #数据导入
                echo "---${tank}---${fileday}----${behavior}--${i}---------运行的语句:LOAD DATA LOCAL INFILE '$myFile' INTO TABLE ${arr[i]} fields terminated by'|' escaped by '^' ---";
            /opt/mysql/bin/mysql -h$mysql_host -u$mysql_user -p$mysql_password  -e "LOAD DATA LOCAL INFILE '$myFile' INTO TABLE ${arr[i]} fields terminated by'|' escaped by '&'  ENCLOSED BY '\"' "
        fi  
    done
    echo -e "\n";
done
date '+%Y-%m-%d %H:%M:%S'


# sleep 3

# /bin/bash /opt/www/server/player_behavior_new.sh &> /opt/www/server/player_behavior_new_$(date +\%Y\%m\%d).log
