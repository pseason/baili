#coding=utf-8
import threading;
import time,datetime;
import commands;
import MySQLdb;
import math
import sys,getopt;
import urllib;
import urllib2;
import json;
# set the default encoding to utf-8
# reload sys model to enable the getdefaultencoding method.
reload(sys);
# using exec to set the encoding, to avoid error in IDE.
exec("sys.setdefaultencoding('utf-8')");
assert sys.getdefaultencoding().lower() == "utf-8";



#-d 2017-02-06 05
d=time.time()-3600;
opts, args = getopt.getopt(sys.argv[1:],"d:",["ifile=","ofile="])
for opt, arg in opts:
  if opt in ("-d", "--ifile"):
    c = time.strptime(arg,"%Y-%m-%d %H")
    d = int(time.mktime(c))
t=time.localtime(d);
date=time.strftime("%Y-%m-%d %H",t);
h=time.strftime("%H",t);
day=time.strftime("%Y-%m-%d",t);

if day==(time.strftime("%Y-%m-%d",time.localtime(time.time()))):
    game_suffix="";
else:
    game_suffix="."+day;
#从线上拉取前一个小时日志线程写入
#array [ {游戏信息1},{游戏信息2},{游戏信息3}]


def get_tankinfo():
    json_str='[{"serverIp":"10.135.126.204","dbName":"zombie_28"}]';
    return json.loads(json_str)



def Import_data(array):
    	#拉取数据
    global t;
    global date;
    global h;
    global day;
    global game_suffix;
    for x in array:
    	#创建文件夹
    	file=x['dbName']+"/"+day+"/game_"+h+".log";
        (status, output)=commands.getstatusoutput("mkdir -p /opt/backup/log/"+x['dbName']+"/"+day);
        (status, output)=commands.getstatusoutput("rm -f /opt/backup/log/"+file);
        #解压
        (status, output)=commands.getstatusoutput("ssh -p 8022 "+x['serverIp']+" \"tar zxvf /opt/www/server/"+x['dbName']+"/log/game.log"+game_suffix+".tar.gz  -C /opt/www/server/"+x['dbName']+"/log \"");
        #ssh -p 8022 10.104.139.129 gunzip /opt/www/server/tank_1/log/game.log.2017-01-10.gz
        # sh="ssh -p 8022 "+x['serverIp']+" gunzip /opt/www/server/"+x['dbName']+"/log/game.log"+game_suffix+".gz";
        #ssh -p 8022 10.104.139.129 cat /opt/www/server/tank_1/log/game.log.2016-11-26 |grep "2016-11-26 01" > /opt/backup/log/tank_1/game_01.log
        # (status, output)=commands.getstatusoutput(sh);
        #拉数据
    	sh="ssh -p 8022 "+x['serverIp']+" cat /opt/www/server/"+x['dbName']+"/log/game.log"+game_suffix+" | grep \"^"+date+"\" | grep -v resourceTimeAdd > /opt/backup/log/"+file;
    	#ssh -p 8022 10.104.139.129 cat /opt/www/server/zombie_1/log/game.log.2016-11-26 |grep "2016-11-26 01" > /opt/backup/log/tank_1/game_01.log
        (status, output)=commands.getstatusoutput(sh);
    	# fo = open(x['dbName']+"/game.log", "w");
    	# fo.write("server:"+str(x['serverId'])+"\n\rdate:"+date+"\n\rsh:"+sh+"\n\r");
    	# fo.close();
    	print(file,x['serverIp'],x['dbName'])






results = get_tankinfo();
#线程次数
thread_count=15;
count=len(results);
Modulo=int(math.ceil(count/float(thread_count)));
array=[[] for i in range(thread_count)];
i=0;
#把数组分成
#[
#   [{游戏信息1},{游戏信息2},{游戏信息3},....],      线程1运行
#   [{游戏信息1},{游戏信息2},{游戏信息3},....]       线程2运行
#]
for k,v in enumerate(results):
	if k % Modulo == 0:
		i+=1;
	array[(i-1)].append(v);
threads = [];
#循环加入线程
for val in array:
    t1 = threading.Thread(target=Import_data,args=(val,))
    threads.append(t1)

if __name__ == '__main__':
    for t in threads:
        t.setDaemon(True)
        t.start()
    for i in threads:
        i.join()
    # Import_data(server_list);
    # print("all over %s" %ctime())
