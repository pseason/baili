# coding=utf8
import os
import paramiko
import MySQLdb
import MySQLdb.cursors
import urllib
import json
import hashlib
import multiprocessing
from concurrent.futures import ThreadPoolExecutor, wait


class LogToDatabase(object):
    def __init__(self, dbIp, dbName, userName="root", password='jeC02GfP'):
        self.dbIp = dbIp
        self.dbName = dbName
        self.userName = userName
        self.password = password

        self.honor = MySQLdb.connect(
            host=self.dbIp,
            user=self.userName,
            passwd=self.password,
            db=self.dbName,
            charset='utf8',
            use_unicode=True,
            cursorclass=MySQLdb.cursors.DictCursor
        )

        self.conn = MySQLdb.connect(
            host="10.66.197.134",
            user="root",
            passwd="jeC02GfP",
            db="honor_gm",
            charset='utf8',
            use_unicode=True,
            cursorclass=MySQLdb.cursors.DictCursor
        )

    def process(self):
        # get honor p_lord data
        print "select data start ...",self.dbName
        self.honor.ping(True)
        self.honor.autocommit(True)
        curs = self.honor.cursor()
        curs.execute("select * from p_lord")
        result=curs.fetchall()

        print "select data over ..."

        self.conn.ping(True)
        self.conn.autocommit(True)
        cur = self.conn.cursor()
        # insert into honor_gm
        print "insert data to honor_gm start ..."
        for line in result:
            cur.execute("delete from p_lord WHERE lordId=%s",(line['lordId']))
            cur.execute(
                "insert p_lord set lordId=%s, nick=%s, portrait=%s, sex=%s, camp=%s, level=%s, exp=%s, " +
                "vip=%s, vipExp=%s, topup=%s, area=%s, pos=%s, gold=%s, goldCost=%s, goldGive=%s, power=%s," +
                "ranks=%s, exploit=%s, job=%s, fight=%s, newState=%s, newerGift=%s, onTime=%s, olTime=%s, offTime=%s," +
                " ctTime=%s, olAward=%s, olMonth=%s, silence=%s, combatId=%s, heroToken=%s, mouthCardDay=%s,mouthCLastTime=%s," +
                " credit=%s, refreshTime=%s, signature=%s, honor=%s, goldBar=%s",
                (line['lordId'],line['nick'],line['portrait'],line['sex'],line['camp'],line['level'],line['exp'],
                 line['vip'],line['vipExp'],line['topup'],line['area'],line['pos'],line['gold'],line['goldCost'],line['goldGive'],line['power'],
                 line['ranks'],line['exploit'],line['job'],line['fight'],line['newState'],line['newerGift'],line['onTime'],line['olTime'],line['offTime'],
                 line['ctTime'],line['olAward'],line['olMonth'],line['silence'],line['combatId'],line['heroToken'],line['mouthCardDay'],line['mouthCLastTime'],
                 line['credit'],line['refreshTime'],line['signature'],line['honor'],line['goldBar']
                 )
            )
        print "insert data to honor_gm over ..."
        curs.close()
        self.honor.close()
        cur.close()
        self.conn.close()


url = "http://127.0.0.1:9400/transfer_honor/shell/all/server"
json_data = json.loads(urllib.urlopen(url).read())
# json_data = json.loads('[{"password":"jeC02GfP","dbIp":"10.66.188.171","userName":"root","dbName":"honor_170"}]')


def fun(item):
    c = LogToDatabase(item['dbIp'], item["dbName"],item['userName'], item["password"])
    c.process()


pool = ThreadPoolExecutor(multiprocessing.cpu_count())
futures = []
for item in json_data:
    futures.append(pool.submit(fun, item))
print(wait(futures, timeout=None, return_when='ALL_COMPLETED'))
